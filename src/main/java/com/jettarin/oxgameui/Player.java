package com.jettarin.oxgameui;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author NITRO 5
 */
public class Player  implements Serializable{
    private char name;
  private  int win;
  private  int lose;
  private  int draw;

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLose() {
        return lose;
    }

    public void setLose(int lose) {
        this.lose = lose;
    }

    public int getDraw() {
        return draw;
    }
    public void win(){
        win++;
    }
    public void lose(){
        lose++;
    }
    public void draw(){
        draw++;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }
    
    public Player(char name){
    this.name = name;    
    }
    
    

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }


    
}
        